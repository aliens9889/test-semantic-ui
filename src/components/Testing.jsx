import React from 'react'
import {
    Button, Container, Divider, Form, Grid, Header, Icon, Image, Item, Label, Loader, Menu, Message, Segment, Step, Table,
} from 'semantic-ui-react'





const Testing = () => (

    <div>
        <div>
            <Container>
                <Grid columns={2}>
                    <Grid.Column
                        className='column-left'
                        width={7}
                    >
                        {/*<Image src='https://vignette.wikia.nocookie.net/warnerbroscartoons/images/8/8e/Dexter-dexter%27s-laboratory-main-character.png/revision/latest?cb=20160613005414' />*/}
                        <Header
                            as='h1'
                            content='Welcome!'
                            textAlign='center'
                            style={{color: '#fefefe'}}
                        />
                        <Header
                            as='h2'
                            content='Applicant Information'
                            textAlign='center'
                            style={{color: '#fefefe'}}
                        />
                    </Grid.Column>


                    <Grid.Column className='column-right' width={9}>
                        <Image src='https://snag.gy/v41QfD.jpg'centered size='small' />
                        <Form>
                            <Form.Group widths='equal'>
                                <Form.Input placeholder='Individual Borrower Name' />
                                <Form.Input placeholder='Individua Borrower Last name' />
                            </Form.Group>
                        </Form>
                        <Form>
                            <Form.Group widths='equal'>
                                <Form.Input placeholder='CUIT' />
                                <Form.Input placeholder='Cell phone' />
                            </Form.Group>
                        </Form>
                        <Form>
                            <Form.Group widths='equal'>
                                <Form.Input placeholder='Email' />
                            </Form.Group>
                        </Form>
                        <Form>
                            <Form.Group widths='equal'>
                                <Form.Input placeholder='Loan Value' />
                                <Form.Input placeholder='Asset Value' />
                                <Form.Select placeholder='Car age' />
                                <Form>
                                    <Loader active inline='centered' />
                                </Form>

                            </Form.Group>
                        </Form>


                        <Header
                            as="h3"
                            content="Plans"
                            textAlign="center"
                            style={{color: '#304650'}}
                        >
                        </Header>
                        <Header
                            as="h4"
                            content="Choose your plan to continue"
                            textAlign="center"
                            style={{color: '#304650'}}
                        >
                        </Header>

                        <Grid textAlign='center' columns={4}>
                            <Grid.Row>
                                <Grid.Column>
                                    <Menu fluid vertical secondary>
                                        <Menu.Item className='table-title'>Term</Menu.Item>
                                        <Menu.Item className='table-title'>Weekly Payment</Menu.Item>
                                        <Menu.Item className='table-title'>Minimun net income</Menu.Item>
                                    </Menu>
                                </Grid.Column>
                                <Grid.Column>
                                    <Menu fluid vertical secondary>
                                        <Menu.Item className='table-title'>24 months</Menu.Item>
                                        <Menu.Item as="a" className='table-content'>
                                            88,61 UVAS
                                            <Divider/>
                                            $1.754,55
                                            <Divider/>
                                            $30.579,31
                                        </Menu.Item>
                                    </Menu>
                                </Grid.Column>
                                <Grid.Column>
                                    <Menu fluid vertical secondary>
                                        <Menu.Item className='table-title'>36 months</Menu.Item>
                                        <Menu.Item as='a' className='table-content'>
                                            88,61 UVAS
                                            <Divider/>
                                            $1.754,55
                                            <Divider/>
                                            $30.579,31
                                        </Menu.Item>
                                    </Menu>
                                </Grid.Column>
                                <Grid.Column>
                                    <Menu fluid vertical secondary>
                                        <Menu.Item className='table-title'>48 months</Menu.Item>
                                        <Menu.Item as="a" className='table-content'>
                                            88,61 UVAS
                                            <Divider/>
                                            $1.754,55
                                            <Divider/>
                                            $30.579,31
                                        </Menu.Item>
                                    </Menu>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>

                        <br/>
                        <br/>
                        <div className="container-apply-btn">
                            <Button className="apply-btn" disabled>APPLY</Button>
                        </div>

                    </Grid.Column>
                </Grid>

            </Container>
        </div>
    </div>

);

export default Testing